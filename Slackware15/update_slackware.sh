#!/bin/bash

# Intro message
echo "Thank you for using Gosh-Its-Arch Slackware Updater"

# Check if the script is being run as root
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root" 
    exit 1
fi

# Run slackpkg commands
echo "Updating..."
slackpkg update
sleep 1

echo "Installing new packages..."
slackpkg install-new
sleep 1

echo "Upgrading all packages..."
slackpkg upgrade-all
sleep 1

echo "Cleaning system..."
slackpkg clean-system
sleep 1

# Run lilo
echo "Running lilo..."
lilo

echo "Slackware fully updated. Reboot your system."
