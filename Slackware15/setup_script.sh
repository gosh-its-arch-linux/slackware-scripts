#!/bin/bash

echo "Thank you for using Gosh-Its-Arch Slackware new user and default runit script"
echo "------------------------------------------------------------"
echo ""

# Check if script is run as root
if [ "$EUID" -ne 0 ]; then
  echo "Please run the script as root."
  exit
fi

# Ask for the username
read -p "Enter the username you wish to add: " username

# Check if the username already exists
if id "$username" &>/dev/null; then
  echo "User $username already exists. Exiting."
  exit 1
fi

# Add the user with the provided username
useradd -m -g users -G wheel,floppy,audio,video,cdrom,plugdev,power,netdev,lp,scanner -s /bin/bash "$username"

# Ask the user to set the password for the new user
echo "Please set the password for '$username' user:"
passwd "$username"

# Change the default run level from 3 to 4 in /etc/inittab
if [ -f "/etc/inittab" ]; then
  sed -i 's/id:3:initdefault:/id:4:initdefault:/g' /etc/inittab
  echo "Default run level changed to 4 in /etc/inittab"
else
  echo "/etc/inittab file not found. Skipping the run level change."
fi

echo "Thank you, the script is completed. Please reboot the system."
