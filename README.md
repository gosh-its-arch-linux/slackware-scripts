# Slackware Scripts


## Getting started

These scripts are meant to make Slackware Linux easier to manage / get up and running!

Remember you using these at your own risk. These are not official scripts please don't ask Slackware Linux team for help.

Scripts must be run as root!

## Steps Slackware 15 (64 bit)

Open up a terminal (be root) and run 

- git clone https://gitlab.com/gosh-its-arch-linux/slackware-scripts.git
- cd slackware-scripts
- cd Slackware15
- chmod +x *.sh


Once completed you should be able to run the individual scripts (All must be run as root)

## Steps Slackware Current (64 bit)

Open up a terminal (be root) and run 

- git clone https://gitlab.com/gosh-its-arch-linux/slackware-scripts.git
- cd slackware-scripts
- cd Current
- chmod +x *.sh

## What each file does

- setup_script -> Use this after installing Slackware and you need to create a non root user + configure Slackware to boot KDE automatically

- update_mirror_and_pkgs -> Once booted into Slackware run this script to configure Slackpkg mirror and to update the cache

- update_slackware -> This will do a full update of Slackware and re-run lilo at the end so you can still boot the system

- install_sbopkg_and_sbotools -> This will automatically download and configure sbotools along with sbopkg so you can install applications via sboinstall easily along with dependency management. 



## Slackware 15 vs Current

- If you using Slackware 15 then use the scripts found in the Slackware15 folder
- If you using Slackware Current (basically) use the scripts found in the Current folder. 
- If you make a mistake you can damage your install
- Slackware 15 and Current are configured for different mirrors as well as sbotools is configured for correct edition of Slackware.
