#!/bin/bash

# Check if the script is running as root
if [ "$EUID" -ne 0 ]; then
    echo "Please run this script as root."
    exit 1
fi

# Welcome message
echo "Welcome to Gosh-Its-Arch Slackware automated sbotools installer"

# Define the URL and the filename
URL="https://github.com/sbopkg/sbopkg/releases/download/0.38.2/sbopkg-0.38.2-noarch-1_wsr.tgz"
FILENAME="sbopkg-0.38.2-noarch-1_wsr.tgz"

# Download the file
wget $URL

# Check if the file was downloaded successfully
if [ $? -ne 0 ]; then
    echo "Error: File could not be downloaded."
    exit 1
fi

# Install the package
installpkg $FILENAME

# Check if the installation was successful
if [ $? -ne 0 ]; then
    echo "Error: Package could not be installed."
    exit 1
fi

echo "Package installed successfully!"

# Run the command to sync with the repository
echo "Syncing with the repository..."
sbopkg -r

# Check if the syncing was successful
if [ $? -ne 0 ]; then
    echo "Error: Syncing with the repository failed."
    exit 1
fi

# After syncing, install sbotools
echo "Installing sbotools..."
sbopkg -i sbotools

# Check if the installation was successful
if [ $? -ne 0 ]; then
    echo "Error: sbotools could not be installed."
    exit 1
fi

echo "sbotools installed successfully!"

# Configure sbotools to use the specified repository
echo "Configuring sbotools repository..."
sboconfig -r https://github.com/Ponce/slackbuilds.git

# Check if the configuration was successful
if [ $? -ne 0 ]; then
    echo "Error: Failed to configure sbotools repository."
    exit 1
fi

# Fetch the latest snapshot
echo "Fetching latest snapshot with sbosnap..."
sbosnap fetch

# Check if the fetch was successful
if [ $? -ne 0 ]; then
    echo "Error: Failed to fetch latest snapshot."
    exit 1
fi

echo "Snapshot fetched successfully!"
echo "All installed, enjoy using Slackware!"
