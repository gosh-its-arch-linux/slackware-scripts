#!/bin/bash

# Ensure script is running as root
if [ "$EUID" -ne 0 ]; then
    echo "Please run this script as root."
    exit 1
fi

MIRROR_FILE="/etc/slackpkg/mirrors"
TARGET_MIRROR="http://mirrors.us.kernel.org/slackware/slackware64-current"

echo "Thank you for using Gosh-Its-Arch Slackware mirror updater"

# Check if the file exists
if [ ! -f "$MIRROR_FILE" ]; then
    echo "Error: $MIRROR_FILE not found."
    exit 1
fi

# Uncomment the specific mirror with spaces handled
sed -i "s|# *[ \t]*\($TARGET_MIRROR\)|\1|" "$MIRROR_FILE"

echo "Mirror uncommented if it was previously commented!"

# Update GPG key and then the package list
slackpkg update gpg && slackpkg update

echo "GPG and package list updated!"

echo "Script complete. Please remember to update Slackware!"
